# Terraform Deployment
This repository contains Terraform and Ansible configuration, which deploys [dr-emu](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed) into OpenStack or VMware vSphere.

All files and documentation for both platforms can be found in the respective folders ([openstack/](./openstack/) and [vsphere](./vsphere)).
