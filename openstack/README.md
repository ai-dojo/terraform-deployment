# Terraform Deployment Using OpenStack
Here is a guide on how to deploy [dr-emu](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed) using Terraform and OpenStack.

## Requirements
- [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)  (v1.6.2)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html) (2.15.7)
- [Ansible community.docker module](https://galaxy.ansible.com/ui/repo/published/community/docker/) (3.8.0)
- access to an OpenStack cloud project
- access to VPN MUNI or eduroam

## Usage
**Note:** This repository is used by [dr-emu](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed)'s pipeline automatically. If you want to test it locally follow the steps below.
1. Delete `backend.tf` from this directory. File `backend.tf` is used to store a state file in Gitlab. However, presence of the file will cause an error when Terraform is run locally (without proper Gitlab configuration).
2. Log in to OpenStack web interface and create application credential. (If you are from MUNI, ask project manager for access to OpenStack dashboard.) You have to fill in a name and select all three roles. Then, save `clouds.yaml` file into this folder.
   ![Create application credential in OpenStack](./images/create_application_credential.png)
3. Create SSH key pair.
   ```bash
   ssh-keygen
   ```
4. Check Terraform variables. For MUNI environment they are already defined in [`muni.tfvars`](./muni.tfvars). Variables' declarations with descriptions can be found in [`variable.tf`](./variables.tf).
5. Add environment variables with paths to your keys:
   ```bash
   export TF_VAR_public_key=path/to/your/key.pub  # required
   export TF_VAR_private_key=path/to/your/key     # optional - used for generating Ansible command
   export TF_VAR_gitlab_token=token               # optional - token for cyst/cyst-core repository
   ```
   `TF_VAR_public_key` is required because Terraform use it to create the compute instance. Other variables are optional and are used only to generate the Ansible command. Not specifying them means that Terraform cannot generate a command for running the Ansible playbook, but all Openstack resources will still be created.
6. Set other required environment variables:
   ```Bash
   export TF_VAR_compute_instance_name="terraform-dr-emu" # NOTE: you can change the name
   export TF_VAR_cloud="openstack"
   export TF_VAR_compute_instance_image_name="ubuntu-jammy-x86_64-*"
   export TF_VAR_compute_instance_user="ubuntu"
   export TF_VAR_compute_instance_flavor="csirtmu.medium4x8"
   export TF_VAR_volume_size=20
   export TF_VAR_network_name="public-private-network"
   export TF_VAR_floating_ip_pool="private-muni-10-16-116"
   export TF_VAR_secgroup_name="ai-dojo-dr-emu-secgroup"
   ```
7. Initialize Terraform:
   ```bash
   terraform init
   ```
8. Run Terraform and preview the deployment:
   ```bash
   terraform apply
   ```
   This command does not make any changes. It just previews changes to be done. To proceed and make these changes, type *yes*.
9. Terraform will run Ansible deployment automatically. However, Terraform still outputs this command in case you would need to rerun it manually. Run this command to install Docker and dr-emu to the created compute instance.

   The command will look like this:
   ```bash
   ANSIBLE_HOST_KEY_CHECKING=False && ansible-playbook -i "<IP>," -u <username> --private-key <private key path> -e "gitlab_user=<gitlab_user> token=<gitlab_token>" dremu_ansible.yaml
   ```
   **Note:** If Ansible throws error UNREACHABLE, it's probably because VM is not ready yet. Waiting about 2 minutes is sufficient to fix this issue. Another cause of this problem could be VPN. You should be connected to VPN MUNI to be able to access the VM.
10. Terraform outputs also IP and username, so you can use SSH to connect to the VM.
11. To continue with dr-emu follow the instructions [here](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed/-/tree/13-demo-2023?ref_type=heads#cli).
12. To destroy the VM run:
    ```bash
    terraform destroy
    ```
## Terraform tfstate
The state of a terraform deployment is saved in the `terraform.tfstate` file locally. If more users want to maintain one deployment, they must share this file. However, since this file contains sensitive information, it should not be in the repository. Users can either send the file directly to each other or establish a shared cloud storage.

## Openstack Security Groups
The compute instance created by Terraform has assigned a security group specified by the mane in `muni.tfvars`. The security group opens ports 22 and 8000, which are accessible only from MUNI network. Security group contains the following rules:

| Direction | Ether Type | IP Protocol | Port Range | Remote IP Prefix |
|-----------|------------|-------------|------------|------------------|
| Egress    | IPv4       | Any         | Any        | 0.0.0.0/0        |
| Egress    | IPv6       | Any         | Any        | ::/0             |
| Ingress   | IPv4       | TCP         | 22 (SSH)   | 147.251.0.0/16   |
| Ingress   | IPv4       | TCP         | 8000       | 147.251.0.0/16   |

