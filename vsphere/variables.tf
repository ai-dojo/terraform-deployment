variable "vsphere_server" {
  description = "vSphere server URL"
  type        = string
}

variable "user" {
  description = "username for vSphere server"
  type        = string
}

variable "password" {
  description = "user's password for vSphere server"
  type        = string
  sensitive   = true
}

variable "vm_name" {
  description = "VM's name"
  type        = string
  default     = "terraform_demo"
}

variable "datacenter" {
  description = "vSphere Datacenter name"
  type        = string
  default     = "Datacenter"
}

variable "datastore" {
  description = "vSphere Datastore name"
  type        = string
}

variable "network" {
  description = "VM's network name"
  type        = string
}

variable "template" {
  description = "template's name"
  type        = string
}

