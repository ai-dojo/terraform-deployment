# VM Networking
This file describes how to connect a VM in vSphere at [vcenter-vgpu-priv.csirt.muni.cz](vcenter-vgpu-priv.csirt.muni.cz) to the network and Internet. This guide can be used to configure new VMs created without a template or for setting up new templates.

## Network diagram
![vSphere and network diagram](images/network_diagram.png)

## Network adapter
Check that the VM is connected to the `VM internal` network. The network adapter is set to `VM internal`, and its status is *connected*.

## Configure a network using Netplan
In the VM's console:
1. Go to the Netplan folder.
   ```bash
   cd /etc/netplan/
   ```
2. Edit file called `00-installer-config.yaml` or similar to contain the following.
   ```yaml
   network:
     ethernets:
       ens33:
         dhcp4: no
         addresses: [192.168.1.20/24] # VM's IP address
         routes:
           - to: default
             via: 192.168.1.1
         nameservers:
           addresses:
           - 147.251.4.33  # ns.muni.cz
     version: 2
   ```
   *Note:* For VM at *vcenter-vgpu-priv.csirt.muni.cz* on *VM internal* network you can use addresses from range *192.168.1.20–192.168.1.50*. For *VM Private MUNI* network use one of these addresses: *10.16.20.205*, *10.16.20.206* and *10.16.20.207*. However, check that the address is not used by other VMs.
3. Apply changes. It will connect the VM to the private network.
   ```bash
   sudo netplan apply
   ```

After this step, the VM should be accessible via **ssh** from VPN MUNI or eduroam networks.

## Configure proxy
We will use a proxy to access the Internet. Proxy's URL is [http://proxy.ics.muni.cz:3128](http://proxy.ics.muni.cz:3128) ([147.251.6.170:3128](http://proxy.ics.muni.cz:3128)).
1. Edit file `/etc/environment`. Add the following lines.
   ```bash
   http_proxy="proxy.ics.muni.cz:3128"
   https_proxy="proxy.ics.muni.cz:3128"
   ftp_proxy="proxy.ics.muni.cz:3128"
   no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com"
   HTTP_PROXY="proxy.ics.muni.cz:3128"
   HTTPS_PROXY="proxy.ics.muni.cz:3128"
   FTP_PROXY="proxy.ics.muni.cz:3128"
   NO_PROXY="localhost,127.0.0.1,localaddress,.localdomain.com"
   ```
2. Configure a proxy for apt by creating a file called `95proxies` in `/etc/apt/apt.conf.d/` containing the following lines.
   ```
   Acquire::http::proxy "http://proxy.ics.muni.cz:3128";
   Acquire::ftp::proxy "ftp://proxy.ics.muni.cz:3128";
   Acquire::https::proxy "http://proxy.ics.muni.cz:3128";
   ```

## Reboot
Reboot the VM.
```bash
sudo shutdown -r now
```
After rebooting, it should connect to the Internet. Try, for example, `sudo apt-get update`.