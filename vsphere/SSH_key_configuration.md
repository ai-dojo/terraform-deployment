# SSH Key Authenticatrion Configuration

1. Generate your SSH keys:
   ```bash
   ssh-keygen
   ```

2. Copy your public key to the VM:
   ```bash
   ssh-copy-id -i /path/to/your/key.pub username@VM
   ```
   for example:
   ```bash
   ssh-copy-id -i ~/.ssh/id_rsa.pub user_template@192.168.1.20
   ```

3. Connect to the VM:
   ```bash
   ssh username@VM
   ```
   for example:
   ```bash
   ssh user_template@192.168.1.20
   ```
   *Note:* SSH will use your key to log in, so you don't need to type a password.

4. Disable password authentication in the VM.
   - Insert line `PasswordAuthentication no` into `/etc/ssh/sshd_config`.
   - Restart SSH service:
     ```bash
     sudo service ssh reload
     ```
5. Disable password for sudo commands
   
   Ansible needs to run commands with sudo privileges without the password.
   - Run:
     ```bash
     sudo visudo
     ```
   - Add the following line:
     ```
     user_template ALL=(ALL) NOPASSWD:ALL
     ```

Now the VM accepts only key authentication for SSH and is ready for Ansible provisioning. More about Ansible can be found in [ansible/](./ansible/) folder.