# Ansible provisioning
Files in this folder configure Ansible to provision *terraform_demo* VM, install Docker, and clone [branch demo-2023 from dr-emu](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed/-/tree/demo-2023?ref_type=heads) into it.

## Requirements
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html) (2.15.7)
- Deployed and running VM *terraform_demo* with default options
- configured SSH key authentication (the guide is available [here](../SSH_key_configuration.md))

## Usage
### vSphere
Check that all variables in `hosts` file are set correctly. IP address should be your VM's IP and *ansible_user*
corresponds to the username of the user in the VM.

Run Ansible playbook `dremu.yaml`:
```bash
ansible-playbook -i hosts dremu.yaml
```

The playbook installs and sets up Docker and clones [branch demo-2023 from dr-emu](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed/-/tree/13-demo-2023?ref_type=heads)
into the home directory of *user_template* and run docker compose up.

### OpenStack
The approach is almost the same as for vSphere.
Check that all variables in `hosts_openstack` file are set correctly. IP address should be your VM's IP and
*ansible_user* corresponds to the username of the user in the VM. You should also specify path to your SSH private key,
which you obtain from OpenStack.

Run Ansible playbook `dremu_openstack.yaml`:
```bash
ansible-playbook -i hosts_openstack dremu_openstack.yaml
```

The playbook installs and sets up Docker and clones [branch demo-2023 from dr-emu](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed/-/tree/13-demo-2023?ref_type=heads)
into the home directory of *user_template* and run docker compose up.

**Note:** Playbooks for vSphere and OpenStack differ in their Docker and proxy settings. vSphere needs proxy 
configuration and OpenStack don't. On the other hand, Docker MTU must be changed for OpenStack.

## Links
- [Docker installation](https://docs.docker.com/engine/install/ubuntu/)
- [Docker daemon and proxy server configuration](https://docs.docker.com/config/daemon/systemd/#manually-creating-the-systemd-unit-files)
- [dr-emu repository](https://gitlab.ics.muni.cz/ai-dojo/docker-testbed)