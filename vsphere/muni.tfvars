# Variables for machines created at vcenter-vgpu-priv.csirt.muni.cz

vsphere_server = "vcenter-vgpu-priv.csirt.muni.cz"
user = "administrator@vsphere.local"
datastore = "Local_storage"
network = "VM internal"
template = "ubuntu-22.04_template_new"
