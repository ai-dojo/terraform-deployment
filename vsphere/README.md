# Terraform Deployment
This demo Terraform deployment creates one virtual machine from a template in a vSphere environment.

## Requirements
- [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)  (v1.6.2)
- server with VMware vSphere environment

## Usage
1.  Initialize Terraform and install vSphere provider (plugin) using:
    ```bash
    terraform init
    ```

2.  Set up variables.  
    You can use `muni.tfvars` file which contains values for vSphere server at [vcenter-vgpu-priv.csirt.muni.cz](vcenter-vgpu-priv.csirt.muni.cz).

    Another option is to use environment variables. Environment variables must be named *TF_VAR_* followed by the name of a declared variable.
    
    Required:
    ```
    vsphere_server  # vSphere server URL (without https://)
    user            # username to access vSphere server
    password        # password of vSphere user
    datastore       # vSphere Datastore name
    network         # vSphere network name
    template        # VM template name
    ```

    Optional:
    ```
    Datacenter      # vSphere Datacenter name, default: "Datacenter"
    vm_name         # VM name, default: "terraform_demo"
    ```

    `password` variable is not set in `muni.tfvars`. There are two ways how to set it. The first one is to use environment variable:
    ```bash
    export TF_VAR_password="password_here"
    ```
    The second one is to set it in interactive Terraform CLI in the next step.

3.  Preview the deployment using:
    ```bash
    terraform apply -var-file="muni.tfvars"
    ```
    If you don't set up the password yet, Terraform will ask you to fill it in now.

    This command does not make any changes. It just previews changes to be done. To proceed and make these changes, type `yes`.

4.  Check the VM.

    Check that VM was created and is running using vSphere Client in your browser or SSH.

    You can log in the VM via web console or connect from your terminal. If you use template `ubuntu-22.04_template_new` from `vcenter-vgpu-priv.csirt.muni.cz` your VM has the following credentials:
    ```
    user:      user_template
    password:  popular-all-listen-tune
    ```
5. SSH

    To access VM via SSH you need to use sshuttle and jump host.

    - Contact the administrator to add your public key to the jump host.
    - Install [sshuttle](https://github.com/sshuttle/sshuttle).
    - In separate terminal run:
        ```bash
        sshuttle --dns -r jump@vpnbridgevgpu.csirt.muni.cz 192.168.1.0/24
        ```
        **Note:** In some cases sshuttle is not able to find your keys which means it cannot connect. In that case specify path to your keys in `~/.ssh/config` file. The file will look like this:
        ```
        Host jump
            HostName vpnbridgevgpu.csirt.muni.cz
            User jump
            IdentityFile <path_to_your_key>
        ```
        It also simplifies the sshuttle command:
        ```
        sshuttle --dns -r jump 192.168.1.0/24
        ```
    - In another terminal window connect via SSH:
        ```bash
        ssh user_template@192.168.1.20
        ```

    Then **configure SSH key authentication**. You can follow the guide [SSH_key_configuration.md](SSH_key_configuration.md).

6.  To destroy the VM run:
    ```bash
    terraform destroy -var-file="muni.tfvars"
    ```

## Terraform tfstate
The state of a terraform deployment is saved in the `terraform.tfstate` file locally. If more users want to maintain one deployment, they must share this file. However, since this file contains sensitive information, it should not be in the repository. Users can either send the file directly to each other or establish a shared cloud storage.

## Network
Network configuration for the VM is described in [VM_networking.md](VM_networking.md). However, template `ubuntu-22.04_template_new` from `vcenter-vgpu-priv.csirt.muni.cz` is already configured.

## Ansible
VM created by Terraform can be provisioned by Ansible. Simple Ansible playbook and documentation can be found in [ansible/](./ansible/).
